<?php
/*
 * $Header: svn+ssh://svn@www.esplanner.com/ESPlannerWeb-drupal/trunk/sites/all/modules/p3p/class/class.p3p.php 11945 2013-02-16 21:37:07Z munroe $
 */

define("p3p_default", "IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT") ;

class p3p {
  static public function ClassPath(
    $theFile = '') {
    return _p3p_ClassPath($theFile) ;
  }

  static public function CssPath(
    $theFile = '') {
    return
      drupal_get_path(
        'module',
        'p3p')
      . '/css/' . $theFile ;
  }

  static public function GetP3PHeader() {
    return sprintf('CP="%s"', variable_get("p3p_settings", p3p_default)) ;
  }

  static public function GetP3PSettings() {
    return explode(" ", variable_get("p3p_settings", p3p_default)) ;
  }

  static public function SetP3PSettings(
    $theSettings) {
    variable_set("p3p_settings", implode(" ", $theSettings)) ;
    return $theSettings ;
  }
}
